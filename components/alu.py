from myhdl import *
from .mux import mux_2, mux_4
from .adder import full_adder_1bit


def alu_1bit(i_a, i_b, i_c, i_m, o_g, o_p, o_y):
    """
    ALU 1-bit block.

    :param i_a:     input operand A,
    :param i_b:     input operand B,
    :param i_c:     input carry and operand B negation control,
    :param i_m:     final multiplexer control,
    :param o_g:     output carry generation,
    :param o_p:     output carry propagation,
    :param o_y:     output result.
    """
    bb = Signal(False)

    b_negate = mux_2(i_b, not i_b, i_c, bb)



    def alu_logic():
        pass

    return alu_logic
