from myhdl import *


def mux_2(i_a, i_b, i_sel, o_z):
    """
    2x1 multiplexer.

    :param i_a:     input A,
    :param i_b:     input B,
    :param i_sel:   control input,
    :param o_z:     output.
    """
    @always_comb
    def mux_logic():
        if i_sel == 0:
            o_z.next = i_a
        else:
            o_z.next = i_b

    return mux_logic


def mux_4(i_a, i_b, i_c, i_d, i_sel, o_z):
    """
    4x1 multiplexer.

    :param i_a:     input A,
    :param i_b:     input B,
    :param i_c:     input C,
    :param i_d:     input D,
    :param i_sel:   control input,
    :param o_z:     output.
    """
    @always_comb
    def mux_logic():
        if i_sel == 0:
            o_z.next = i_a
        elif i_sel == 1:
            o_z.next = i_b
        elif i_sel == 2:
            o_z.next = i_c
        else:
            o_z.next = i_d

    return mux_logic
