from myhdl import *


def half_adder_1bit(i_a, i_b, o_c, o_s):
    """
    Single-bit half adder.

    :param i_a:     input A,
    :param i_b:     input B,
    :param o_c:     output carry (generation),
    :param o_s:     output sum.
    """
    @always_comb
    def adder_logic():
        o_s.next = i_a ^ i_b
        o_c.next = i_a & i_b

    return adder_logic


def full_adder_1bit_carry(i_a, i_b, i_c, o_c, o_s):
    """
    Single-bit full adder with sequential carry output and g&p.

    :param i_a:     input A,
    :param i_b:     input B,
    :param i_c:     input carry,
    :param o_c:     output carry,
    :param o_s:     output sum.
    """
    im_s, im_c1, im_c2 = [Signal(False) for i in range(3)]

    half_adder_1 = half_adder_1bit(i_c, i_a, im_c1, im_s)
    half_adder_2 = half_adder_1bit(im_s, i_b, im_c2, o_s)

    @always_comb
    def carry_logic():
        o_c.next = im_c1 | im_c2

    return half_adder_1, half_adder_2, carry_logic


def lookahead_adder(i_a, i_b, i_c, o_g, o_p, o_s):
    """
    Carry lookahead adder cell (CLC). Taken from
    http://www.ece.lsu.edu/ee3755/2013f/cla.pdf

    :param i_a:     input operand A,
    :param i_b:     input operand B,
    :param i_c:     input carry,
    :param o_g:     output carry generation signal,
    :param o_p:     output carry propagation signal,
    :param o_s:     output sum.
    """
    @always_comb
    def clc_logic():
        o_g.next = i_a & i_b
        o_p.next = i_a | i_b
        o_s.next = i_a ^ i_b ^ i_c

    return clc_logic


def lookahead_cgl(i_c, i_g, i_p, o_c):
    """
    Carry generation logic.

    :param i_c:     input carry signal,
    :param i_g:     input carry generation bus,
    :param i_p:     input carry propagation bus,
    :param o_c:     output carry bus.
    """
    # get device bit width
    cgl_width = len(i_g)

    # convert input buses into individual signals lists
    im_gl = [i_g(i) for i in range(cgl_width)]
    im_pl = [i_p(i) for i in range(cgl_width)]

    # create intermediate output carry signals list
    im_cl = [Signal(False) for i in range(cgl_width)]

    # represent it as a bus
    im_c = ConcatSignal(*reversed(im_cl))

    @always_comb
    def carry_logic():
        for i in range(cgl_width):
            im_cl[i].next = im_gl[i] | any([(im_gl[j-1] if j > 0 else i_c) & all(im_pl[j:i+1]) for j in range(i+1)])
        o_c.next = im_c

    return carry_logic


def cl_adder(i_a, i_b, i_c, o_c, o_s):
    """
    Variable width lookahead-carry adder.

    :param i_a:     input operand A bus,
    :param i_b:     input operand B bus,
    :param i_c:     input carry signal,
    :param o_c:     output carry signal,
    :param o_s:     output sum bus.
    """
    # get device bit width
    adder_width = len(i_a)

    # convert input buses into individual signals lists
    im_al = [i_a(i) for i in range(adder_width)]
    im_bl = [i_b(i) for i in range(adder_width)]

    # create intermediate output carry signals list
    im_gl = [Signal(False) for i in range(adder_width)]
    im_pl = [Signal(False) for i in range(adder_width)]
    im_sl = [Signal(False) for i in range(adder_width)]

    # create intermediate carry bus (between ALU cells and CGL)
    im_c = Signal(intbv(0)[adder_width:])

    # represent intermediate signal lists as a buses
    im_g = ConcatSignal(*reversed(im_gl))
    im_p = ConcatSignal(*reversed(im_pl))
    im_s = ConcatSignal(*reversed(im_sl))

    cgl = lookahead_cgl(i_c, im_g, im_p, im_c)

    cells = [lookahead_adder(im_al[i],
                             im_bl[i],
                             im_c(i-1) if i > 0 else i_c,
                             im_gl[i],
                             im_pl[i],
                             im_sl[i]) for i in range(adder_width)]

    @always_comb
    def adder_logic():
        o_s.next = im_s
        # assign last bit of intermediate carry bus to output carry signal
        o_c.next = im_c(adder_width-1)

    return cells, cgl, adder_logic
