from random import randrange
from myhdl import *

from components.adder import cl_adder


a = Signal(intbv(0)[8:])
b = Signal(intbv(0)[8:])
ci = Signal(False)
co = Signal(False)
s = Signal(intbv(0)[8:])

#toVerilog(cl_adder, a, b, ci, co, s)


def test_bench():
    dut = cl_adder(a, b, ci, co, s)

    @instance
    def test():
        yield delay(10)
        for i in range(1000):
            a.next = randrange(0, 255)
            b.next = randrange(0, 255)
            ci.next = bool(randrange(0, 2))
            yield delay(10)
            print('~~~~~~~~~~~~~')
            print(a, b, ci)
            print(co, s)
            print('~~~~~~~~~~~~~')
            assert a + b + int(ci) == s + co * 256

    return dut, test

sim = Simulation(test_bench())
sim.run()
